
<?php

if (!is_file("config.php")) {
  header("location:install.php");
}
require_once 'functions.php';
$conn = db_get_connection();
if (isset($_GET["sort"])) {
  $sort = $_GET["sort"];  
} else {
  $sort = "DESC";
}
if (isset($_GET['pageno'])) {
  $pageno = $_GET['pageno'];
} else {
  $pageno = 1;
}                   
$n = 20;
$offset = ($pageno - 1) * $n;
$total_pages = page_counter($n, $conn);                 
$data = all_post_display($conn, $offset, $n, $sort);                   
?>





<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-10 mx-auto">
      <center>
      <div class="btn btn-dark" >
      <form name="frm1" method="GET" >
      <?php
      if(isset($_GET['pageno'])) {
        $pno=$_GET['pageno'];
      }
      else{
        $pno=1;   
      }
      ?>

      <input type="hidden" value="<?php echo $pno; ?>" name="pageno">

      <select  name="sort" onchange="this.form.submit()">
      <option value="">select order</option>
      <option value="DESC">Desending orderby date</option>
      <option value="ASC">Asending orderby date</option>
      </select>
    


      </form>

      </div>
      </center>

      <?php 
        if (isset($data)) {
          foreach ($data as $row) {
            $val = $row["bid"];
            $str = $row["content"];
            $cont = content_trimmer($str);
            echo '
                    <div class="post-preview"> 
                      <a href="sql.php?id='.$val.'">
                      <h2 class="post-title">'.$row["title"].'</h2>
                      <h3 class="post-subtitle">'.$cont.'</h3>
                      </a><p class="post-meta">Posted by
                      <a href="#">Start Bootstrap</a>
                      on '.$row["date"].'</p>
                      ';
            $data2 = tag_display($val, $conn);
              echo "<p>Tags: ";
              if (isset($data2)) { 
                foreach ($data2 as $row2) {
                  $tagidval = $row2["tid"];
                  echo '<a href="tag.php?tag=' . $tagidval . '">#' . $row2["tags"] . ' </a>';
                }
              }
              echo "</p>
                      </div>
                      <hr>";
          }
        } 
        else {
              echo "0 results";
        }
      ?>

        <!-- Pager -->
        <div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if($pageno == 1){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if($pageno == 1){ echo '#'; } else { echo "index.php?sort=$sort&pageno=".($pageno - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if($pageno == $total_pages){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if($pageno == $total_pages){ echo '#'; } else { echo "index.php?sort=$sort&pageno=".($pageno + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
