<?php

  require_once 'functions.php';
  if(isset($_GET["status"])) { 
    echo '<script language="javascript">alert("Updated Successfully")</script>';
  }
  $conn = db_get_connection();         
  $idval = $_GET["id"];
  if(filter_var($idval, FILTER_VALIDATE_INT)){
    $row = post_display($conn, $idval);
  }
?>





<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
            </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/post-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
          
          <?php          
            if (isset($row)) {
              echo '
                       <h1>'.$row["title"].'</h1>
                       <h2 class="subheading">Currently No Subheading....</h2>
                       <span class="meta">Posted by
                       <a href="#">Start Bootstrap</a>
                       on '.$row["date"].'</span>
                       <div class="container">
                       <div class="row">
                      
                     ';          
            }
            else {
                 echo "0 results";
            }
          ?>

            
          </div>
        </div>
      </div>
    </div>
  </header>

 <!-- Button for edit option -->
 <?php 

  echo '

    <div >
      <center>
        <a href="editpost.php?id='.$idval.'"> 
          <button  type="submit" class="btn btn-primary" name="editblog" id="EditBlogButton">Edit</button>
        </a>
      </center>
    </div>
    ';


 ?>



<!--     -->
   

  <!--Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
       
        <p>
          <?php 
          echo $row["content"]; 
          ?>
        </p>                     
        <p>
          <?php
            $data2 = tag_display($idval, $conn);
              echo "<p>Tags: ";
              if (isset($data2)) { 
                foreach ($data2 as $row2) {
                  $tagidval = $row2["tid"];
                  echo '<a href="tag.php?tag='.$tagidval.'">#'.$row2["tags"].' </a>';                      
                }
              }
          ?>
        </p>
        
        </div>
      </div>
    </div>
  </article>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
