<?php 

  function gen($n){
    $b = array(  'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
            'a', 'ac', 'accumsan', 'ad', 'aenean', 'aliquam', 'aliquet', 'ante',
            'aptent', 'arcu', 'at', 'auctor', 'augue', 'bibendum', 'blandit',
            'class', 'commodo', 'condimentum', 'congue', 'consequat', 'conubia',
            'convallis', 'cras', 'cubilia', 'curabitur', 'curae', 'cursus',
            'dapibus', 'diam', 'dictum', 'dictumst', 'dignissim', 'dis', 'donec',
            'dui', 'duis', 'efficitur', 'egestas', 'eget', 'eleifend', 'elementum',
            'enim', 'erat', 'eros', 'est', 'et', 'etiam', 'eu', 'euismod', 'ex',
            'facilisi', 'facilisis', 'fames', 'faucibus', 'felis', 'fermentum',
            'feugiat', 'finibus', 'fringilla', 'fusce', 'gravida', 'habitant',
            'habitasse', 'hac', 'hendrerit', 'himenaeos', 'iaculis', 'id',
            'imperdiet', 'in', 'inceptos', 'integer', 'interdum', 'justo',
            'lacinia', 'lacus', 'laoreet', 'lectus', 'leo', 'libero', 'ligula',
            'litora', 'lobortis', 'luctus', 'maecenas', 'magna', 'magnis',
            'malesuada', 'massa', 'mattis', 'mauris', 'maximus', 'metus', 'mi',
            'molestie', 'mollis', 'montes', 'morbi', 'mus', 'nam', 'nascetur',
            'natoque', 'nec', 'neque', 'netus', 'nibh', 'nisi', 'nisl', 'non',
            'nostra', 'nulla', 'nullam', 'nunc', 'odio', 'orci', 'ornare',
            'parturient', 'pellentesque', 'penatibus', 'per', 'pharetra',
            'phasellus', 'placerat', 'platea', 'porta', 'porttitor', 'posuere',
            'potenti', 'praesent', 'pretium', 'primis', 'proin', 'pulvinar',
            'purus', 'quam', 'quis', 'quisque', 'rhoncus', 'ridiculus', 'risus',
            'rutrum', 'sagittis', 'sapien', 'scelerisque', 'sed', 'sem', 'semper',
            'senectus', 'sociosqu', 'sodales', 'sollicitudin', 'suscipit',
            'suspendisse', 'taciti', 'tellus', 'tempor', 'tempus', 'tincidunt',
            'torquent', 'tortor', 'tristique', 'turpis', 'ullamcorper', 'ultrices',
            'ultricies', 'urna', 'ut', 'varius', 'vehicula', 'vel', 'velit',
            'venenatis', 'vestibulum', 'vitae', 'vivamus', 'viverra', 'volutpat',
            'vulputate',
    );
        
    $rand=array(); 
    shuffle($b);    
    for($i=0;$i<$n;$i++){
        $rand[$i]=$b[$i];
    }
    $rand= implode(" ",$rand);
    return $rand;
  }

  function db_get_connection() {  
    require_once "config.php";
    static $db;
    try {
      if (!isset($db)) {
        $db = new PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } 
    } catch(PDOException $ex) {
        echo "Connection failed! " . $ex->getMessage();
    }
    return $db;
  }


  function add_post($conn, $title, $blogg, $tag, $currentdate)	{
    $tag = trim($tag, " ");
    $tag = strtolower($tag);
    $words = explode(" ", $tag);
    $s = sizeof($words);
    for ($i = 0; $i < $s; $i++) {
      $words[$i] = trim($words[$i]);
    }
    try { 
      $blogg = addslashes($blogg);
      $sql = "INSERT INTO blog (title, content, date) VALUES ('$title', '$blogg', '$currentdate')";
      $conn->exec($sql);
      for ($i =0; $i < $s; $i++) {
        $resl = $conn->query("SELECT  tags FROM tag  WHERE  tags = '$words[$i]'");
        $count = $resl->rowCount();
        if ($count == 0) {
          $tagsq = "INSERT INTO tag (tags)  VALUES ('$words[$i]')";
          $conn->exec($tagsq);
        }
      $relquery = "INSERT INTO reltab(blogid, tagid)
      SELECT bbid.bid, ttid.tid
      FROM blog bbid JOIN	tag ttid
      ON bbid.title = '$title' AND ttid.tags = '$words[$i]'";
      $conn->exec($relquery);
      }
      return TRUE;
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
  }  



  function add_tables($dbname, $username, $password, $dummyno) {
    try {
      $conn1 = new PDO("mysql:host=localhost", $username, $password);
      $conn1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sql = "DROP DATABASE IF EXISTS " . $dbname . ";";
      $conn1->exec($sql);
      $db = "CREATE DATABASE " . $dbname;
      $usedb = "USE " . $dbname; 
      $conn1->exec($db);
      $conn1->exec($usedb);
      $table1 = "CREATE TABLE blog (
                bid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                title TEXT NOT NULL,
                content TEXT NOT NULL,
                date DATE NOT NULL)";    
      $conn1->exec($table1);    
      $table2 = "CREATE TABLE tag (
                tid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                tags varchar(100) NOT NULL UNIQUE)";    
      $conn1->exec($table2);
      $table3 = "CREATE TABLE reltab (
                 blogid INT(11) NOT NULL,
                tagid INT(11) NOT NULL,
                FOREIGN KEY (blogid) REFERENCES blog (bid),
                FOREIGN KEY (tagid) REFERENCES tag (tid))";
      $conn1->exec($table3);      
      if (isset($_POST['dummynumber'])) {
        for ($k = 0; $k < number_format($dummyno); $k++) {
          $dummycurrentdate = date("Y-m-d H:i:s");
          $dummyt = gen(4);
          $dummyb = gen(100);
          $dummyta = gen(2);
          $words = explode(" ", $dummyta);
          $s = sizeof($words);  
          for($i = 0; $i < $s; $i++) {
            $words[$i] = trim($words[$i]);
          }
          $sql = "INSERT INTO blog (title, content, date) VALUES ('$dummyt', '$dummyb', '$dummycurrentdate')";
          $conn1->exec($sql);  

          // $sql1 = $conn1->prepare("INSERT INTO blog (title, content, date) VALUES (?, ?, ?)");
          // $sql1->execute([$dummyt,$dummyb,$dummydate]);       



          for ($i = 0; $i < $s; $i++) {
            $resl = $conn1->query("SELECT  tags FROM tag  WHERE  tags = '$words[$i]'");      
            $count = $resl->rowCount();   

            // $resl = $conn1->query("SELECT  tags FROM tag  WHERE  tags = ?");    
            // $res1->execute([$words[$i]])  ;
            // $count = $resl->rowCount();   
            
            
            if ($count==0) {
              $tagsq="INSERT INTO tag (tags)  VALUES('$words[$i]')";
              $conn1->exec($tagsq);

              // $tagsq=$conn1->prepare("INSERT INTO tag (tags)  VALUES(?)");
              // $tagsq->execute([$words[$i]]);

            }
            $relquery = "INSERT INTO reltab (blogid, tagid)
                    SELECT bd.bid, tt.tid
                    FROM blog bd JOIN	tag tt
                    ON bd.title = '$dummyt' AND tt.tags = '$words[$i]'";  
            $conn1->exec($relquery);
          }
        }
      }
      //header("location:index.php");
    } catch(PDOException $e) {
      echo "Connection failed: " . $e->getMessage();
    }
  }


  function post_display($conn, $id)	{
    $stmt = $conn->prepare("SELECT  title , content, date FROM  blog WHERE bid=?");
    $stmt->execute([$id]); 
    return 	$stmt->fetch();
    }


  function all_post_display($conn, $offset, $n, $sort)	{
    $stmt = $conn->prepare("SELECT bid, title, content, date FROM blog 
      ORDER BY bid $sort LIMIT $offset, $n");                 
    $stmt->execute();
    $data = $stmt->fetchAll();
    return $data;	
    }


  function tag_display($idval, $conn)	{
    $sql1 = "SELECT tag.tags, tag.tid FROM reltab, tag 
              WHERE reltab.blogid = ? AND tag.tid = reltab.tagid";
    $stmt2 = $conn->prepare($sql1);
    $stmt2->execute([$idval]);
    return $stmt2->fetchAll();
    }


  function tag_post_display($conn, $offset, $n, $sort, $tagidvalue2)	{
    $sql = "SELECT bid, title, content, date FROM blog, reltab 
            WHERE blog.bid = reltab.blogid and $tagidvalue2 = reltab.tagid 
            ORDER BY bid $sort LIMIT $offset, $n";                   
    if (filter_var($tagidvalue2, FILTER_VALIDATE_INT)) {
      $stmt = $conn->prepare($sql); 
      $stmt->execute();
      $data = $stmt->fetchAll();
    }
    return $data;
  }

  function update_post($conn, $title, $blogg, $tag, $idval) {
    $tag = trim($tag, " ");
    $tag = strtolower($tag);
    $words = explode(" ", $tag);
    $s = sizeof($words);
    for ($i = 0; $i < $s; $i++) {
      $words[$i] = trim($words[$i]);
    }
    try {  
      $blogg = addslashes($blogg);
      $sql = "UPDATE blog SET title = '$title', content = '$blogg' WHERE bid = $idval";
      $conn->exec($sql);
      $del = "DELETE FROM reltab
       where blogid = $idval";
      $conn->exec($del);
      for ($i = 0; $i < $s; $i++) {
        $resl = $conn->query("SELECT tags FROM tag  WHERE  tags = '$words[$i]'");
        $count = $resl->rowCount();    
        if ($count == 0) {
          $tagsq = "INSERT INTO tag (tags)  VALUES ('$words[$i]')";
          $conn->exec($tagsq);
        }
        $relquery = "INSERT INTO reltab (blogid, tagid)
                      SELECT bbid.bid, ttid.tid
                      FROM blog bbid JOIN	tag ttid
                      ON bbid.bid = $idval AND ttid.tags = '$words[$i]'";
        $conn->exec($relquery);
      }
      return TRUE;  
    }catch(PDOException $e) {
      echo "Connection failed: " . $e->getMessage();
    }
  }


  function page_counter($n, $conn)	{
    $total_pages_sql = "SELECT bid FROM blog";
    $q1 = $conn->query($total_pages_sql);
    $total_rows = $q1->rowCount();
    $total_pages = ceil($total_rows / $n);
    return $total_pages;
  }

  function content_trimmer($str)	{
    $words = explode(" ", $str);
    $cont =  implode(" ", array_splice($words, 0, 200));
    if (str_word_count($cont) > 199) {
      $cont= $cont."...";
    }
    return $cont;
  }

?>  