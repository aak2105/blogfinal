<?php
require_once 'functions.php';
if (isset($_POST['submitInstall'])) {
  $dbname=$_POST['dbname'];
  $username=$_POST['username'];
  $password=$_POST['password'];
  $dummy=$_POST['dummycheck'];
  $dummyno=$_POST['dummynumber'];
  add_tables($dbname, $username, $password, $dummyno);
  $writer='<?php
    $host = "localhost";
    $username = "'.$username.'";
    $password = "'.$password.'";
    $dbname = "'.$dbname.'";     
    ?>        
    ';
  $myfile = fopen("config.php", "w") or die("can't open file");
  chmod($myfile, 0777);
  fwrite($myfile, $writer);
  fclose($myfile);
  //echo $dummyno;
  header("location:index.php");    
}      
?>



<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

  <!-- script for enabling and disabling textbox for entering 
  number of dummy contents when check box is clicked-->
  <script >
    function EnableDisableTextBox(chkDummyno) {
        var nodummy = document.getElementById("nodummy");
        nodummy.disabled = chkDummyno.checked ? false : true;
        if (!nodummy.disabled) {
            nodummy.focus();
        }
    }
  </script>



</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Start Bootstrap</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
            </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/blog-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Add your database details</h1>
            <span class="subheading"></span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        
        
        <form name="blogform1"  method="POST" action="">
        <!-- <input type="subm" value=" Submit " name="submitInstall" class="btn btn-primary"/> -->
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Title</label>
              <input type="text" class="form-control" placeholder="databasename" name="dbname" required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Title</label>
              <input type="text" class="form-control" placeholder="username" name="username" required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Title</label>
              <input type="text" class="form-control" placeholder="password" name="password" required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
          </div>


          <div class ="control-group">
            <div class="form-group floating-label-form-group controls">
            <h6><input type="checkbox" name="dummycheck" id="check" onclick="EnableDisableTextBox(this)" />
                 Click check box if dummy content is to be inserted?</h6>
              <input type="number" name="dummynumber"  id="nodummy" disabled="disabled" required data-validation-required-message/>
            </div>
          </div>


          <br>

          <div id="success"></div>
              <div class="form-group">
              <input type="submit" value=" Submit " name="submitInstall" class="btn btn-primary"/>
              </div>

        </form>
      
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>


  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>


