-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2019 at 12:42 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aak2105`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `bid` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`bid`, `title`, `content`, `date`) VALUES
(1, 'dictum amet aliquet orci', 'porta imperdiet id cubilia consequat convallis conubia mattis porttitor gravida inceptos suspendisse phasellus faucibus est nascetur facilisi facilisis justo curae fames erat ante ex laoreet bibendum leo aenean fusce euismod tellus lacus per scelerisque torquent amet praesent penatibus natoque commodo consectetur velit pharetra eu a felis nullam venenatis feugiat mollis ridiculus lobortis risus arcu mi ligula curabitur sollicitudin parturient sagittis sodales nulla interdum nec maecenas dictum adipiscing sed himenaeos fringilla integer suscipit sem habitasse at rhoncus sapien tortor fermentum quam malesuada quis primis magnis nunc ullamcorper dolor vel duis aliquam proin orci ut elit turpis ultricies donec cras massa placerat', '2019-11-25'),
(2, 'ullamcorper hac ridiculus non', 'lacus porta enim egestas tellus luctus eu quis morbi ridiculus finibus eget diam sed imperdiet platea justo in conubia auctor tempus lectus erat sociosqu rhoncus augue tristique amet ornare ultricies placerat efficitur torquent quam non elit nunc facilisis pharetra natoque sagittis aliquet scelerisque nullam curae feugiat magna cursus condimentum pellentesque ultrices mi nam dictumst himenaeos ullamcorper etiam vehicula habitant sapien hac gravida facilisi maecenas potenti blandit mauris massa ex quisque nostra netus tempor iaculis euismod vitae risus fames aliquam primis est molestie nibh orci suspendisse bibendum litora consectetur laoreet habitasse suscipit venenatis rutrum vulputate dui ut malesuada consequat class neque', '2019-11-25'),
(3, 'nostra risus molestie nam', 'nunc varius suscipit habitant bibendum feugiat pulvinar pretium netus tortor elementum nascetur nam arcu urna dolor phasellus porta placerat sollicitudin fusce facilisis facilisi aliquet diam interdum malesuada class fermentum ex praesent leo penatibus efficitur nullam mauris neque nec egestas velit aenean dignissim elit luctus vehicula donec torquent aptent tempus viverra dictumst dui eu blandit habitasse ornare maximus volutpat ad montes curae morbi ipsum ut eget adipiscing ligula faucibus eleifend quam mi justo tempor lacinia est potenti parturient at curabitur iaculis lorem lectus rutrum quisque inceptos condimentum erat aliquam hac lobortis conubia sit sem purus etiam suspendisse hendrerit amet dapibus sapien', '2019-11-25'),
(4, 'imperdiet accumsan ac fusce', 'vulputate dictum lacus amet egestas penatibus himenaeos dapibus litora ac nulla augue facilisi ipsum inceptos nisl habitant non curabitur sit venenatis pharetra cubilia dictumst mollis mattis nec sed est nullam euismod posuere nostra nascetur conubia maximus aenean parturient fringilla magnis montes ornare consequat potenti suspendisse tristique porttitor volutpat felis interdum cras class convallis at morbi natoque eleifend ante elementum lacinia aptent platea auctor donec ullamcorper hendrerit molestie sociosqu scelerisque ultrices consectetur aliquam proin libero metus semper dignissim lobortis sagittis sem leo praesent lectus elit senectus hac vitae vehicula curae habitasse pretium primis cursus eu erat viverra dis orci nisi laoreet', '2019-11-25'),
(5, 'cubilia tempus dapibus conubia', 'non volutpat hac cursus dignissim felis consectetur nulla vitae aenean mollis varius fermentum porta conubia nisi est mus nibh lorem ad curae sed iaculis vestibulum pellentesque interdum phasellus accumsan donec quam sollicitudin turpis velit rhoncus et luctus etiam eu tincidunt viverra duis at habitant ornare ex sit primis senectus fusce maximus finibus ac molestie bibendum augue tristique erat sodales eros orci mi enim magna convallis posuere vulputate parturient lacus dictum adipiscing placerat consequat urna congue efficitur faucibus ante malesuada rutrum sem curabitur natoque ipsum aliquam inceptos id tellus vivamus integer mattis gravida risus penatibus arcu suspendisse a laoreet maecenas fames', '2019-11-25'),
(6, 'massa cubilia blandit tristique', 'ultricies viverra habitant cursus proin lacus auctor ultrices neque turpis ac semper risus rhoncus tincidunt commodo vel dui nam cubilia nibh suspendisse torquent tempor cras condimentum bibendum netus sapien pulvinar at nulla ridiculus nascetur velit vehicula in dapibus mauris nisi mi dignissim nullam dolor tempus ut sociosqu inceptos praesent montes malesuada tellus aliquam sodales odio eleifend vulputate lorem consequat phasellus a quisque iaculis porttitor penatibus hac amet sed ex facilisis aliquet etiam orci enim feugiat facilisi sem lacinia donec platea vitae eros ipsum efficitur felis est nostra curabitur taciti tristique accumsan venenatis massa habitasse finibus morbi dictumst scelerisque maecenas interdum', '2019-11-25'),
(7, 'nostra magnis habitant elit', 'commodo imperdiet pharetra pretium euismod quam ac gravida curabitur donec finibus quis habitant lacinia porta tristique est accumsan eget mus praesent leo scelerisque viverra hendrerit netus aliquet mi natoque taciti diam nibh dapibus velit urna lobortis maecenas elementum maximus arcu quisque himenaeos venenatis nullam metus mattis nulla neque ridiculus ex feugiat montes sociosqu molestie rutrum a ligula magnis eu vitae habitasse amet nec fames libero orci tempor suspendisse nam lacus id facilisis rhoncus tellus non pellentesque semper litora cras phasellus sodales in vivamus sagittis purus egestas nunc torquent auctor faucibus efficitur aliquam tincidunt congue aenean pulvinar iaculis sit proin sapien', '2019-11-25'),
(8, 'parturient tempor habitasse nec', 'turpis semper bibendum condimentum libero erat potenti dolor tincidunt orci parturient tellus cubilia fames odio curabitur mi consectetur etiam nisl montes nibh a leo facilisi netus sociosqu fermentum vel nostra cursus pellentesque integer placerat imperdiet dis euismod urna quis tempor venenatis volutpat aliquet id ridiculus ad sed lacinia pulvinar ullamcorper duis adipiscing diam risus magna massa efficitur fusce magnis fringilla augue porta habitasse ligula dapibus cras taciti nascetur nunc sodales rhoncus habitant pharetra felis convallis quam nam vitae porttitor vestibulum malesuada finibus sollicitudin arcu vivamus auctor conubia primis ipsum eleifend faucibus velit lorem vehicula sagittis nisi senectus ultrices accumsan est', '2019-11-25'),
(9, 'justo dolor risus finibus', 'velit nostra mus pretium imperdiet quis ridiculus gravida mollis pellentesque sociosqu scelerisque adipiscing sem condimentum ante pulvinar magna et donec dictum mauris aliquam ad ornare tellus enim potenti torquent neque fermentum duis luctus eleifend lacinia turpis cras lacus maximus sit amet iaculis arcu dictumst at in consequat nisl non eros leo vivamus eget nulla justo orci vel platea ullamcorper semper per curae magnis metus fusce malesuada consectetur nullam blandit commodo aliquet fames natoque euismod diam sed praesent dis etiam sollicitudin dapibus placerat sagittis maecenas sodales lobortis vulputate venenatis litora aenean porttitor ex auctor varius senectus eu augue ut ligula posuere', '2019-11-25'),
(10, 'ante laoreet mus massa', 'enim pretium lorem adipiscing nulla tortor in fames ad tempor litora volutpat elit nisi posuere aenean aptent diam ridiculus lectus nunc tellus praesent nullam mus primis scelerisque consequat a integer habitasse eros feugiat interdum auctor quisque cursus blandit vestibulum mattis euismod pellentesque molestie sapien ipsum quam duis nisl netus efficitur lobortis sed vel imperdiet ac magnis sociosqu tempus dictumst ultrices donec per cubilia curabitur hendrerit fringilla montes nascetur amet ut platea congue vehicula lacus torquent id egestas turpis etiam condimentum nostra vivamus velit magna class mollis suspendisse commodo laoreet pulvinar facilisi phasellus conubia arcu fusce iaculis dapibus metus sollicitudin fermentum', '2019-11-25'),
(11, 'mauris cras malesuada vel', 'cras dignissim porta vulputate aliquam quisque dui erat ex conubia euismod netus eget bibendum et aenean ut per fermentum efficitur libero scelerisque semper facilisi interdum mollis velit maecenas mus primis natoque duis lacinia id finibus habitant pellentesque ad fringilla lorem cubilia nunc dictumst at fames maximus consequat inceptos nisi proin adipiscing senectus integer sed sapien iaculis ullamcorper diam mattis placerat eleifend ultricies penatibus imperdiet felis ligula phasellus leo augue aliquet nascetur porttitor urna congue taciti nullam volutpat habitasse elementum mauris nibh varius tincidunt hac risus quis montes ipsum dapibus dictum dis eros magna eu ridiculus vitae quam suspendisse tellus est', '2019-11-25'),
(12, 'curae ultrices augue nam', 'litora molestie purus integer sed porta justo suscipit per efficitur vehicula mauris rutrum nullam ex velit diam euismod erat aliquet iaculis vulputate blandit cursus ipsum cubilia tristique placerat augue convallis donec ac sit posuere ornare dapibus amet felis nisl pretium faucibus enim in senectus phasellus gravida penatibus lobortis est conubia viverra arcu sem ligula risus ultricies dignissim etiam accumsan magna quis tempor finibus urna turpis commodo nunc auctor praesent interdum feugiat eget suspendisse eu primis scelerisque quam tellus ut adipiscing aliquam volutpat orci dis libero vel maximus nostra habitant odio potenti lacinia congue nec tortor metus class eros pharetra ullamcorper', '2019-11-25'),
(13, 'dignissim ultrices potenti malesuada', 'nunc sed quis imperdiet tellus vel facilisi cursus laoreet mauris nibh ac feugiat tristique porttitor augue nascetur convallis ante ex vehicula euismod aliquet praesent habitant neque vivamus metus at magnis lobortis molestie elit libero lacinia fermentum duis lectus quisque dictum nullam ornare integer vulputate lorem curae felis elementum habitasse nisi himenaeos malesuada mi justo rhoncus conubia consectetur hendrerit potenti volutpat dapibus aliquam proin turpis diam montes nulla adipiscing ut dis morbi pretium finibus placerat nam litora torquent enim accumsan vestibulum non etiam tortor eu iaculis sem rutrum dolor maecenas netus fusce posuere penatibus eros aptent bibendum leo parturient tincidunt pulvinar', '2019-11-25'),
(14, 'efficitur senectus dui etiam', 'ultrices maecenas diam id ultricies sit ante parturient suspendisse eu finibus vestibulum vulputate dignissim rhoncus urna luctus nec hendrerit torquent pretium egestas dictumst consequat ut integer facilisis felis venenatis nullam nibh dolor libero lorem fermentum arcu nisi laoreet nam mollis orci sollicitudin convallis senectus tincidunt at fusce montes ipsum neque efficitur sapien metus enim ac natoque pharetra proin dis magnis aenean netus est porttitor duis scelerisque potenti ad interdum quis porta conubia tortor massa vel aliquet eget maximus nisl odio praesent platea suscipit euismod in a aptent purus lacinia molestie mauris tempor sagittis adipiscing tristique mus phasellus nunc consectetur eleifend', '2019-11-25'),
(15, 'integer maximus mauris augue', 'torquent ultrices fringilla nam nibh nunc id finibus commodo congue mi donec tortor quam tristique arcu mollis tempus facilisi ridiculus himenaeos adipiscing habitasse sem natoque metus ultricies magna mauris convallis lacus non egestas nec ex dictum nisi montes aenean venenatis laoreet mus ac in interdum lacinia gravida vehicula ullamcorper lectus scelerisque magnis a dui senectus posuere sit purus dolor nostra cursus pellentesque quis elit lorem rutrum taciti elementum iaculis orci hendrerit habitant urna semper proin fermentum nascetur neque netus tellus molestie imperdiet et libero aptent blandit est inceptos eros massa ante auctor eget vestibulum aliquet ipsum vulputate maximus varius turpis', '2019-11-25'),
(16, 'nibh dolor pulvinar nisl', 'quis efficitur aliquet non etiam nulla egestas urna fringilla velit congue magna placerat nunc dolor fermentum dictumst dui mattis feugiat ac sapien ipsum aliquam class maximus ridiculus fames curabitur lacinia proin nibh quam per phasellus eleifend potenti adipiscing sodales hac bibendum pharetra ullamcorper consectetur eget vulputate vitae mollis morbi leo ultricies luctus pretium rhoncus euismod himenaeos tempor volutpat ut augue justo sem finibus dapibus a sagittis nascetur consequat aenean purus cubilia iaculis massa lacus diam tristique convallis maecenas suscipit arcu enim porttitor nam scelerisque vel blandit lectus platea orci litora nisi semper faucibus cursus accumsan senectus in eu elementum posuere', '2019-11-25'),
(17, 'elit suscipit himenaeos finibus', 'maximus urna lectus lorem class lobortis ornare nam lacinia auctor torquent venenatis feugiat fermentum inceptos consequat ligula pulvinar et facilisis congue potenti eu volutpat tristique ultrices nisl quisque dictumst senectus vehicula vitae mollis scelerisque mattis sed justo nec maecenas nibh suspendisse primis nullam metus tempus ex vestibulum posuere eros magna pharetra imperdiet dapibus sapien hendrerit egestas risus commodo faucibus duis conubia gravida diam eget consectetur integer tempor tellus platea dis amet sociosqu hac orci dolor ut libero ipsum vulputate massa praesent pretium viverra augue id dignissim bibendum nascetur velit litora phasellus nulla himenaeos per accumsan parturient vel rutrum penatibus est', '2019-11-25'),
(18, 'semper mattis egestas massa', 'est risus eleifend sem efficitur mus dis feugiat dolor nostra elit et lacus luctus tellus montes habitant nascetur cubilia etiam venenatis tortor iaculis potenti lobortis primis scelerisque class rutrum ullamcorper per praesent hac suspendisse consequat odio leo tincidunt aptent urna tempor id integer nulla velit ad at maecenas finibus amet fusce metus dictumst sollicitudin fames lacinia hendrerit nunc commodo viverra vivamus augue sapien eget volutpat placerat congue quisque duis sit ornare pretium in ac turpis euismod dapibus suscipit vulputate tempus vehicula aliquet ante massa fermentum nisl nam ut vel mattis morbi himenaeos vitae platea lectus taciti molestie phasellus torquent vestibulum', '2019-11-25'),
(19, 'ac nullam hendrerit tortor', 'tincidunt ultricies turpis tempus metus rhoncus arcu consequat id nascetur ad libero etiam tellus sed platea nam volutpat euismod at ipsum condimentum cursus non bibendum placerat penatibus porttitor phasellus pretium fringilla nisi sit vehicula sem lectus viverra interdum ullamcorper tristique felis morbi blandit adipiscing risus aliquet convallis amet per sociosqu tortor montes mollis ultrices donec taciti magna ornare nunc diam class augue malesuada duis parturient himenaeos dui nostra eu ex dapibus tempor quam ridiculus habitasse faucibus inceptos curabitur hac semper erat laoreet eleifend pulvinar accumsan eget velit quis ligula lacinia lacus leo conubia lorem suspendisse vel a senectus imperdiet vestibulum', '2019-11-25'),
(20, 'litora pharetra cubilia lectus', 'lorem facilisis netus velit pellentesque massa lobortis habitant aptent dolor et sodales dapibus senectus cras duis volutpat consequat viverra etiam ante porta elementum mi augue quis ipsum tellus at felis eros per sagittis mattis vulputate curabitur malesuada auctor mauris rutrum class turpis condimentum suscipit eget litora vel congue vestibulum nisl elit nam tristique proin parturient suspendisse dis eu sed ac nisi morbi nibh ut molestie libero eleifend varius lectus feugiat neque luctus inceptos magnis non dignissim quisque a magna tempor euismod platea scelerisque fermentum amet ex ullamcorper praesent rhoncus primis vivamus nullam lacus tempus convallis faucibus egestas risus adipiscing fames', '2019-11-25'),
(21, 'interdum sapien mattis tristique', 'risus amet lorem hendrerit tortor orci sollicitudin commodo suscipit porttitor ultrices bibendum tincidunt facilisis leo mollis ipsum mauris sociosqu conubia penatibus neque odio aptent habitasse egestas maximus primis cursus ultricies augue placerat consectetur ornare convallis eros nec congue elementum dignissim dictum vel diam tempus finibus praesent erat vulputate per ex ullamcorper habitant non et auctor etiam eleifend massa aenean adipiscing rutrum lobortis euismod malesuada dapibus ac hac suspendisse nisl volutpat potenti rhoncus urna eget est quam lectus nibh sagittis mi interdum proin sodales taciti netus justo velit litora lacus gravida facilisi sem cubilia magna nisi ante nullam fusce quis venenatis', '2019-11-25'),
(22, 'gravida parturient nisi cubilia', 'porta a eget libero at turpis metus himenaeos egestas dignissim facilisis purus velit eros risus sagittis netus tristique posuere curabitur maximus fermentum iaculis morbi aliquet facilisi praesent neque pharetra dui blandit lobortis natoque elit diam justo integer etiam pellentesque massa nisl accumsan malesuada pretium sodales ultrices imperdiet rhoncus ad aliquam fusce augue sapien curae suscipit erat hendrerit fames est feugiat dictumst nec lectus venenatis per potenti ex nullam convallis faucibus nibh class mauris ante taciti eleifend bibendum tincidunt phasellus litora mattis cubilia inceptos vivamus dis laoreet sit condimentum ornare nulla quam nunc ridiculus elementum mollis luctus conubia habitant mus consequat', '2019-11-25'),
(23, 'proin aptent neque eu', 'facilisi aliquet purus nulla lacinia eu augue curae nostra fames vestibulum ad diam nisi congue mus pharetra porttitor taciti lorem egestas dapibus per integer penatibus tristique ridiculus donec nascetur suspendisse proin litora ultricies a quis neque quisque mi risus nisl odio curabitur placerat magna bibendum rhoncus semper senectus phasellus aenean aptent torquent magnis ultrices vulputate felis conubia inceptos metus blandit vehicula potenti nunc porta luctus urna feugiat varius libero dui duis praesent velit interdum elementum posuere pulvinar ipsum sapien dictum eget euismod fusce consequat ante volutpat nam dictumst elit natoque morbi ligula at nullam maximus sollicitudin efficitur sagittis lacus tempor', '2019-11-25'),
(24, 'tristique aptent cras vehicula', 'neque aliquet consectetur enim elementum commodo porta ac ridiculus mauris molestie malesuada taciti efficitur euismod ex luctus per elit faucibus cras eros purus cursus etiam inceptos accumsan pellentesque habitant hendrerit congue in lectus gravida praesent nibh velit fringilla at sem class quis quam suspendisse libero consequat sagittis lacus pulvinar eget fusce vulputate ultrices suscipit sit metus eu mus ullamcorper leo mattis vitae egestas platea augue aptent morbi et sodales arcu conubia auctor curae dictumst pharetra viverra bibendum mollis feugiat adipiscing lacinia justo lobortis turpis dictum odio pretium nullam penatibus integer vehicula volutpat vestibulum dolor aliquam donec amet parturient iaculis ligula', '2019-11-25'),
(25, 'cubilia per justo dignissim', 'quisque ac porttitor lacus elit habitant euismod dui proin eleifend primis maecenas imperdiet faucibus placerat etiam ad ante natoque ornare integer aliquam lacinia dictumst est nulla ex ultricies eget tempor pretium non nullam vitae parturient class viverra dapibus rhoncus porta eu sed mollis rutrum vulputate libero fringilla ipsum fusce et ut cursus augue dis conubia maximus senectus semper aptent facilisi nisi nascetur laoreet morbi montes gravida tempus nec egestas velit per nostra litora erat neque id tortor consectetur sodales leo inceptos donec netus feugiat curabitur elementum enim orci tincidunt arcu sagittis suscipit consequat sociosqu luctus nibh nam tristique vehicula finibus', '2019-11-25'),
(26, 'bibendum odio lectus sit', 'dignissim id pretium vivamus sapien ridiculus aliquet proin risus hendrerit nam inceptos consectetur ultrices molestie interdum morbi quam himenaeos posuere ac litora justo etiam tristique viverra mauris commodo massa nostra vehicula aliquam tincidunt potenti cubilia vitae nascetur imperdiet mi consequat finibus lectus dui maecenas taciti turpis vel magna ut magnis ex curae aptent nisi sollicitudin sodales netus nunc lobortis laoreet egestas a praesent bibendum senectus auctor montes vulputate tempus congue dictumst malesuada conubia primis eleifend felis diam phasellus rutrum fusce adipiscing donec ullamcorper porta suspendisse luctus sociosqu feugiat odio varius convallis nisl sit erat eget purus habitant dapibus cursus integer', '2019-11-25'),
(27, 'interdum integer posuere ornare', 'non congue mus consectetur faucibus inceptos nam sed imperdiet praesent hac himenaeos fames pharetra urna aliquam et mi curabitur cursus orci arcu maximus penatibus quam pretium magnis placerat sociosqu enim tristique mattis pellentesque hendrerit per id integer cubilia sapien vitae cras mauris duis egestas ultrices curae elementum volutpat massa gravida nulla vulputate nisi finibus iaculis risus malesuada eget nibh bibendum blandit elit porta eros ridiculus eu euismod turpis conubia sodales ac tortor aptent tempus lobortis leo metus facilisi habitant semper maecenas dictum velit vestibulum fermentum vivamus vehicula auctor dictumst augue mollis condimentum laoreet quis etiam tempor litora felis diam in', '2019-11-25'),
(28, 'mi magnis elit ullamcorper', 'volutpat litora metus leo taciti malesuada arcu habitasse odio rhoncus diam nulla viverra faucibus sodales quam lacinia accumsan pulvinar vel aenean ipsum mi luctus proin ullamcorper elementum mus lectus tortor morbi senectus porta etiam mattis adipiscing fusce tellus fringilla euismod scelerisque ridiculus lobortis dignissim nibh cursus curabitur cras suspendisse congue praesent nunc magna ligula eleifend mauris vehicula dictumst suscipit risus massa dolor eros finibus at commodo pharetra efficitur consequat pellentesque facilisis maecenas tempus tincidunt fermentum pretium convallis vivamus sem nisl justo amet sit lorem aliquet molestie montes donec gravida est netus nostra libero sagittis blandit enim quisque placerat phasellus urna', '2019-11-25'),
(29, 'habitant elementum ad suscipit', 'integer quam sit torquent ac netus iaculis sollicitudin nibh convallis dolor dapibus turpis orci lacinia purus id habitasse justo est nulla in blandit laoreet parturient tristique lobortis etiam neque gravida maximus nisi commodo himenaeos morbi volutpat amet lacus curabitur taciti fermentum ridiculus placerat finibus bibendum praesent ligula erat euismod conubia ante tempor luctus eget nunc rhoncus faucibus dignissim lorem sem habitant consequat vitae ipsum ad mauris rutrum risus porttitor quis donec duis malesuada proin sociosqu feugiat enim cubilia mus ullamcorper curae ut a fames diam nascetur mattis hac venenatis libero primis elit sagittis montes hendrerit platea elementum aptent adipiscing litora', '2019-11-25'),
(30, 'interdum quisque senectus aptent', 'pellentesque sagittis a nostra ultrices tincidunt tempor interdum vel litora felis ultricies fusce elit purus aliquam sollicitudin odio velit cubilia fermentum per pretium magna facilisis dui porta eleifend posuere nulla lectus nisi curabitur erat pulvinar egestas malesuada id risus mauris suspendisse sociosqu ridiculus hac in vivamus nec proin montes auctor viverra iaculis augue aptent neque vehicula nunc ac et eros libero dapibus ullamcorper euismod amet dignissim maximus blandit torquent finibus aenean conubia leo tristique himenaeos justo dictum varius feugiat duis habitant morbi rhoncus lacinia lorem hendrerit ad scelerisque suscipit nibh consectetur mus lacus arcu dolor eget penatibus enim taciti dictumst', '2019-11-25'),
(31, 'commodo nam proin consequat', 'netus ultricies suspendisse lacinia dis sociosqu efficitur tempor porta blandit lobortis condimentum auctor libero vitae rutrum etiam hendrerit tristique arcu gravida tortor vulputate mauris aenean vestibulum magnis ante justo ullamcorper neque nullam morbi pulvinar adipiscing viverra ipsum facilisis aliquam iaculis cubilia est egestas dapibus massa litora pharetra facilisi felis lectus velit bibendum duis molestie non ut cras dignissim et ultrices magna orci aptent feugiat consectetur tincidunt augue nisi purus laoreet habitant ex accumsan at ornare curae ac nam dictumst in luctus pellentesque commodo mi vel fermentum odio porttitor aliquet nulla risus semper convallis fringilla cursus suscipit dictum malesuada montes placerat', '2019-11-25'),
(32, 'eu turpis semper aenean', 'pharetra malesuada bibendum euismod integer sociosqu ultricies dui congue nisl et habitasse nunc sagittis potenti dis imperdiet vestibulum parturient aptent felis tristique a eget erat nascetur lacus taciti quisque fermentum est sed id porta nostra tempus sit lacinia tortor ut facilisi etiam magnis cras diam nullam rutrum ridiculus mollis justo pretium finibus faucibus dapibus turpis efficitur natoque habitant orci nec litora scelerisque sem maecenas ex ligula vivamus consequat molestie fusce vitae tellus mattis iaculis ante fames at penatibus dolor porttitor magna odio per leo hac himenaeos nibh adipiscing conubia viverra eu tempor placerat class senectus ad laoreet duis urna fringilla', '2019-11-25'),
(33, 'habitasse purus luctus cras', 'mattis gravida lacinia himenaeos aliquet arcu elit eget lectus senectus laoreet litora metus fames aliquam porta per porttitor iaculis interdum hac ante condimentum commodo tristique sociosqu rutrum nec eu euismod auctor quis adipiscing cubilia fermentum duis vitae molestie maecenas ipsum elementum class nascetur in accumsan tortor tincidunt mus venenatis eros dictum non felis pretium vel tempus efficitur platea luctus integer morbi varius praesent netus faucibus parturient egestas consequat purus erat etiam viverra facilisi fusce potenti posuere magna inceptos quisque donec dolor dignissim amet bibendum at pellentesque maximus leo vulputate suspendisse facilisis nunc feugiat eleifend rhoncus enim nam conubia ridiculus ultrices', '2019-11-25'),
(34, 'porttitor et donec netus', 'ante sem diam facilisis fames risus platea non auctor nisi at scelerisque lectus magna nullam dolor vitae commodo suspendisse habitant ut class dictum nibh maecenas ullamcorper dis massa porta potenti ultrices augue morbi porttitor nulla etiam per dictumst sit natoque malesuada hendrerit tempus aptent est consectetur lobortis enim mauris primis sapien adipiscing pharetra iaculis litora torquent hac gravida dui luctus tortor aliquam proin sed facilisi eu nostra lacus phasellus cubilia egestas duis conubia montes arcu vulputate blandit semper nisl sodales leo neque habitasse sociosqu quisque efficitur nunc ridiculus dapibus ac vehicula finibus tempor imperdiet faucibus pellentesque fusce nascetur lacinia feugiat', '2019-11-25'),
(35, 'habitant accumsan montes inceptos', 'placerat felis ac netus odio vulputate leo sollicitudin condimentum augue ridiculus himenaeos imperdiet adipiscing auctor metus litora rhoncus pharetra at volutpat cursus ad sodales quam neque nostra urna curae suspendisse senectus vivamus mi dictum euismod velit per orci et quis potenti finibus duis ultrices molestie conubia fames mauris rutrum facilisi iaculis nascetur convallis facilisis porttitor vestibulum dis accumsan diam est id platea ante efficitur mollis in taciti consectetur eros aptent dignissim primis dictumst mus arcu lacus vel purus class ullamcorper congue maecenas nam blandit elementum gravida amet hac tempor eget nulla nisl ultricies etiam pellentesque praesent turpis risus eleifend quisque', '2019-11-25'),
(36, 'eros nisi erat ornare', 'diam pharetra blandit rutrum integer sapien semper fames eu tristique inceptos eros maecenas sollicitudin facilisi at class ornare aenean donec dignissim enim conubia eget facilisis ullamcorper interdum orci primis iaculis gravida lobortis nec volutpat netus cubilia adipiscing aliquam ad nulla ex turpis platea egestas malesuada dis ac ridiculus torquent posuere imperdiet in rhoncus odio augue suspendisse curae nunc taciti pretium cras viverra penatibus auctor phasellus potenti ligula arcu non senectus felis massa consectetur sagittis ante tincidunt nisl habitant euismod lectus venenatis quisque ultrices metus scelerisque vitae lacus litora efficitur a dictumst condimentum sit elementum duis justo tortor faucibus fringilla nam', '2019-11-25'),
(37, 'ac montes finibus consequat', 'duis proin interdum lacinia phasellus nunc fermentum per habitasse dis felis nisl nullam volutpat varius turpis justo elit dignissim tellus massa rhoncus faucibus suspendisse commodo fusce dictum netus non aenean feugiat rutrum iaculis a litora dui hac enim nisi sociosqu curabitur pretium dolor purus scelerisque montes facilisis egestas cras vestibulum vivamus etiam placerat sagittis ac vel laoreet conubia finibus natoque odio pharetra vulputate amet facilisi primis blandit torquent morbi tortor molestie bibendum ullamcorper aliquet auctor magna sodales luctus mollis hendrerit fringilla curae inceptos id tempus taciti urna dictumst nam tempor ad cubilia fames lacus eget posuere sapien et suscipit diam', '2019-11-25'),
(38, 'justo luctus vestibulum quis', 'dui netus dignissim a at egestas mus accumsan curabitur taciti praesent habitasse in neque sapien potenti metus risus nibh quisque velit tempor dictum suscipit tortor venenatis odio congue imperdiet pretium duis lacus senectus porta luctus quam consequat tempus rutrum mauris mattis scelerisque semper ut dis finibus quis diam lorem ad porttitor platea mi sociosqu dolor orci parturient ultricies non pharetra tellus vulputate integer inceptos maecenas curae adipiscing lobortis purus arcu massa donec ridiculus malesuada et dictumst molestie tincidunt leo viverra iaculis nullam nec suspendisse litora class volutpat pellentesque magnis enim erat fringilla posuere maximus eu id libero urna sagittis est', '2019-11-25'),
(39, 'diam fames curae himenaeos', 'parturient urna ut aliquet sapien eleifend habitant efficitur tincidunt cras enim magna curae dapibus diam nulla facilisi congue sociosqu morbi consequat odio non purus pharetra fermentum venenatis senectus iaculis phasellus mollis egestas molestie et semper rhoncus integer mauris laoreet erat primis viverra aptent potenti elit nostra eros orci consectetur inceptos suspendisse lacus proin magnis sit ridiculus luctus at malesuada felis velit maximus torquent curabitur lobortis in duis scelerisque dis etiam maecenas varius natoque praesent ante ligula vel commodo hac nullam penatibus placerat sagittis mattis pulvinar ornare mi a finibus tempus nec tristique auctor quisque augue posuere montes aenean dictum nisl', '2019-11-25'),
(40, 'conubia ultricies varius fames', 'suspendisse massa habitant gravida cursus pellentesque pretium curae cubilia vulputate montes condimentum primis ante dui commodo vitae sit himenaeos imperdiet risus semper tristique parturient natoque porta taciti hac adipiscing phasellus accumsan eleifend pharetra nam ultrices eros fames aliquam turpis ridiculus nostra nibh sed id sociosqu facilisis velit tempor neque maximus mattis dignissim mi luctus mus vel quis quam congue lacinia libero consequat class eget vehicula auctor tempus sollicitudin morbi senectus ligula duis felis urna elementum convallis torquent sodales tincidunt potenti efficitur platea iaculis penatibus metus at bibendum fermentum donec habitasse tellus per vestibulum purus enim malesuada ex magnis viverra faucibus', '2019-11-25'),
(41, 'lacinia netus ullamcorper quisque', 'ligula tempus placerat viverra montes porta nisl morbi justo lorem aptent facilisis non donec orci blandit erat pharetra facilisi adipiscing fermentum risus eu fusce dignissim convallis magna venenatis a nostra luctus suscipit arcu sit lacus nisi mattis senectus enim dictumst porttitor quis auctor maecenas quam duis inceptos semper hendrerit rutrum aliquam odio potenti at curabitur scelerisque bibendum tristique feugiat hac et tortor nullam lacinia egestas ex praesent ante nunc habitasse finibus ullamcorper fringilla aenean aliquet sapien molestie euismod consectetur diam turpis ridiculus massa class ultricies sociosqu cubilia metus vestibulum natoque eget purus sed dui vitae etiam primis nibh vulputate phasellus', '2019-11-25'),
(42, 'non placerat commodo orci', 'lectus commodo in non felis eleifend pellentesque at cursus consequat potenti nisi tortor condimentum justo ultricies nostra quam lacinia consectetur laoreet augue vivamus libero porta orci phasellus praesent habitasse mollis ultrices etiam quis iaculis viverra nunc montes donec eros ullamcorper lacus netus facilisis aliquam leo nulla nibh sed nec sapien rutrum accumsan morbi parturient porttitor sodales natoque placerat hendrerit bibendum mattis convallis purus platea habitant fermentum ridiculus pharetra diam class ipsum lobortis neque tempus integer mauris primis ad egestas lorem ut sem molestie dis enim rhoncus sagittis velit dictumst per scelerisque auctor vulputate odio volutpat mus luctus congue sociosqu nisl', '2019-11-25'),
(43, 'magnis sodales venenatis eget', 'in leo mi arcu blandit vulputate semper nibh tristique iaculis odio vitae imperdiet per hac nunc morbi placerat torquent enim lacinia suscipit facilisis ultrices ad ac libero diam nulla venenatis massa magna ante vivamus quam varius feugiat velit auctor interdum elementum vehicula mattis proin porttitor neque ex vel sociosqu pharetra purus taciti inceptos fermentum sodales platea dolor at rhoncus tempor quis molestie maximus nisl facilisi amet conubia elit aliquet nec fringilla consectetur euismod rutrum ultricies viverra efficitur metus risus duis egestas sem et a curabitur sit erat fusce tincidunt pellentesque ut ligula phasellus maecenas dis tortor consequat nascetur augue mauris', '2019-11-25'),
(44, 'mauris mattis nec convallis', 'vivamus elementum feugiat pretium nostra sodales nisi rhoncus nullam lectus platea augue ut condimentum lacus convallis molestie odio quisque iaculis felis tempor metus leo senectus viverra netus mattis congue ante integer in morbi habitant cursus sagittis massa vestibulum finibus habitasse nunc blandit dis vel placerat nec enim justo aptent orci pulvinar varius interdum volutpat potenti cras conubia pellentesque aenean risus mauris dictumst porttitor ornare sapien eu imperdiet auctor nulla efficitur sem tortor est elit proin tristique aliquam facilisi eget adipiscing fringilla arcu ipsum aliquet dui mollis duis vehicula cubilia bibendum penatibus semper tincidunt montes ullamcorper nascetur mi scelerisque dignissim fermentum', '2019-11-25'),
(45, 'ultricies rhoncus suspendisse sodales', 'convallis cubilia turpis quis curabitur dictum elementum gravida dignissim lectus lorem leo tempus luctus ullamcorper nostra tellus aenean rutrum tortor nunc ligula sagittis pretium amet fusce feugiat bibendum risus facilisi at accumsan commodo lacus blandit proin vulputate tempor ut dapibus venenatis efficitur aptent dolor condimentum netus ridiculus purus facilisis velit himenaeos dictumst urna molestie congue lobortis felis aliquam malesuada hendrerit imperdiet nec consectetur quisque donec mi posuere vitae habitasse mauris vivamus ac nisl per lacinia massa varius augue vel egestas morbi orci maecenas praesent maximus fringilla litora suscipit adipiscing porta libero neque laoreet fermentum conubia vehicula sollicitudin id pulvinar sodales', '2019-11-25'),
(46, 'himenaeos curae rhoncus urna', 'euismod aptent urna dui magna nullam nunc nam ac elit sociosqu turpis ultrices dictum condimentum etiam amet accumsan porttitor massa libero est ipsum iaculis ad parturient torquent blandit mi ridiculus erat dolor duis curae viverra pretium neque ullamcorper at enim ultricies aliquam semper proin ut himenaeos faucibus volutpat ante felis orci conubia elementum mollis adipiscing sapien vivamus gravida venenatis leo tempus placerat malesuada magnis risus platea non rhoncus velit diam lectus odio nascetur mus lacus feugiat maximus dignissim vestibulum donec litora fames integer per habitant nibh posuere quam molestie augue montes sem cursus suspendisse class eget lorem senectus in praesent', '2019-11-25'),
(47, 'quisque cubilia lacus consequat', 'semper laoreet duis scelerisque aptent elit quam dictum ipsum augue porta rutrum dignissim placerat morbi ullamcorper ultricies sollicitudin volutpat ridiculus quisque consequat pharetra hendrerit facilisis etiam potenti sapien adipiscing justo risus tempus mi magna luctus convallis leo conubia turpis nam venenatis nullam fames libero vel sagittis nisl montes inceptos suscipit maecenas praesent himenaeos mus proin hac lacus cubilia dapibus malesuada neque erat ex nisi ante facilisi efficitur id mollis imperdiet nec sit a nostra ligula maximus torquent phasellus eget mauris sociosqu consectetur ut auctor nibh lectus vitae sem litora orci eros ultrices et dictumst magnis iaculis odio cras tincidunt fringilla', '2019-11-25'),
(48, 'maecenas eleifend dapibus varius', 'nascetur rhoncus ad aptent lorem conubia pharetra habitant semper auctor himenaeos magna mi eu vivamus fusce habitasse nisi cursus felis dictum nibh elit viverra feugiat molestie hac ullamcorper natoque sed sem etiam purus magnis quisque parturient montes consectetur justo nulla platea proin ut phasellus inceptos cubilia porttitor per posuere tellus aliquam mollis nec lacus sociosqu egestas litora sit vel massa id luctus efficitur erat fermentum tempor dictumst suspendisse nullam donec accumsan ac venenatis varius amet suscipit pellentesque vulputate dui facilisis vestibulum in placerat a duis est nam sagittis fringilla arcu taciti elementum dolor vitae aenean lacinia aliquet malesuada ultricies dignissim', '2019-11-25'),
(49, 'natoque hendrerit suscipit etiam', 'lectus volutpat facilisis curae egestas efficitur parturient natoque class luctus primis imperdiet vitae magnis pharetra ullamcorper elementum phasellus hendrerit himenaeos risus nunc massa potenti semper leo orci convallis penatibus sodales torquent per faucibus vulputate dictum gravida commodo sollicitudin tortor diam morbi aliquet odio mollis scelerisque taciti libero habitasse arcu venenatis aliquam platea montes varius augue tellus donec netus dignissim viverra duis placerat eget consectetur a praesent bibendum purus hac ipsum ut dictumst elit nullam consequat vestibulum cras interdum etiam maecenas sem eu ac nibh molestie habitant tincidunt sociosqu nulla porta mauris mi et ultrices aenean est sapien urna suspendisse nisl', '2019-11-25'),
(50, 'bibendum cursus egestas neque', 'semper ad eleifend elit purus dignissim fusce magna suscipit vivamus massa lorem porta enim potenti gravida habitant conubia nibh condimentum cursus ligula mi litora nec tincidunt mus leo vestibulum duis tempus ante velit id nisi scelerisque congue amet vulputate curabitur blandit laoreet vitae nulla non penatibus nostra senectus lacinia cras praesent per malesuada facilisis habitasse sed primis integer dolor augue curae efficitur fringilla iaculis torquent orci porttitor arcu aliquet egestas molestie dictum magnis sagittis nisl odio pellentesque bibendum dapibus erat diam sem suspendisse auctor netus mauris justo consectetur vehicula aenean ridiculus tristique turpis nunc parturient libero nam finibus metus quam', '2019-11-25');

-- --------------------------------------------------------

--
-- Table structure for table `reltab`
--

CREATE TABLE `reltab` (
  `blogid` int(11) NOT NULL,
  `tagid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reltab`
--

INSERT INTO `reltab` (`blogid`, `tagid`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 5),
(3, 6),
(4, 7),
(4, 8),
(5, 9),
(5, 10),
(6, 11),
(6, 12),
(7, 13),
(7, 14),
(8, 15),
(8, 16),
(9, 17),
(9, 18),
(10, 19),
(10, 20),
(11, 21),
(11, 22),
(12, 23),
(12, 24),
(13, 25),
(13, 26),
(14, 27),
(14, 28),
(15, 29),
(15, 30),
(16, 31),
(16, 32),
(17, 33),
(17, 34),
(18, 35),
(18, 36),
(19, 37),
(19, 38),
(20, 31),
(20, 39),
(21, 40),
(21, 41),
(22, 23),
(22, 42),
(23, 43),
(23, 44),
(24, 3),
(24, 7),
(25, 45),
(25, 46),
(26, 47),
(26, 48),
(27, 37),
(27, 3),
(28, 49),
(28, 50),
(29, 51),
(29, 45),
(30, 37),
(30, 52),
(31, 53),
(31, 54),
(32, 55),
(32, 56),
(33, 57),
(33, 58),
(34, 59),
(34, 60),
(35, 61),
(35, 62),
(36, 63),
(36, 64),
(37, 65),
(37, 26),
(38, 66),
(38, 15),
(39, 8),
(39, 67),
(40, 66),
(40, 37),
(41, 68),
(41, 29),
(42, 69),
(42, 70),
(43, 32),
(43, 61),
(44, 59),
(44, 71),
(45, 5),
(45, 72),
(46, 67),
(46, 73),
(47, 74),
(47, 75),
(48, 76),
(48, 77),
(49, 22),
(49, 45),
(50, 78),
(50, 48);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `tid` int(11) NOT NULL,
  `tags` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`tid`, `tags`) VALUES
(21, 'aliquet'),
(2, 'ante'),
(41, 'aptent'),
(74, 'blandit'),
(30, 'congue'),
(20, 'consectetur'),
(3, 'conubia'),
(40, 'curae'),
(22, 'cursus'),
(26, 'diam'),
(13, 'dictum'),
(25, 'dignissim'),
(10, 'dis'),
(76, 'efficitur'),
(69, 'egestas'),
(68, 'elementum'),
(54, 'erat'),
(42, 'eros'),
(12, 'est'),
(23, 'eu'),
(9, 'ex'),
(65, 'facilisi'),
(63, 'facilisis'),
(64, 'fames'),
(45, 'fringilla'),
(70, 'gravida'),
(37, 'hendrerit'),
(59, 'iaculis'),
(24, 'id'),
(34, 'in'),
(52, 'inceptos'),
(77, 'ipsum'),
(55, 'lectus'),
(51, 'litora'),
(6, 'luctus'),
(58, 'magna'),
(53, 'malesuada'),
(49, 'massa'),
(62, 'mattis'),
(36, 'maximus'),
(48, 'molestie'),
(61, 'nam'),
(50, 'nascetur'),
(39, 'natoque'),
(28, 'nec'),
(11, 'netus'),
(1, 'nisl'),
(8, 'nostra'),
(16, 'nullam'),
(19, 'nunc'),
(46, 'odio'),
(60, 'parturient'),
(75, 'pharetra'),
(72, 'placerat'),
(15, 'platea'),
(43, 'porta'),
(29, 'posuere'),
(66, 'praesent'),
(31, 'pretium'),
(18, 'purus'),
(5, 'quam'),
(38, 'rhoncus'),
(57, 'ridiculus'),
(47, 'risus'),
(14, 'rutrum'),
(7, 'scelerisque'),
(33, 'sem'),
(27, 'sollicitudin'),
(4, 'taciti'),
(17, 'tellus'),
(67, 'tincidunt'),
(56, 'torquent'),
(78, 'ullamcorper'),
(35, 'vehicula'),
(32, 'venenatis'),
(44, 'viverra'),
(71, 'volutpat'),
(73, 'vulputate');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `reltab`
--
ALTER TABLE `reltab`
  ADD KEY `blogid` (`blogid`),
  ADD KEY `tagid` (`tagid`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`tid`),
  ADD UNIQUE KEY `tags` (`tags`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `reltab`
--
ALTER TABLE `reltab`
  ADD CONSTRAINT `reltab_ibfk_1` FOREIGN KEY (`blogid`) REFERENCES `blog` (`bid`),
  ADD CONSTRAINT `reltab_ibfk_2` FOREIGN KEY (`tagid`) REFERENCES `tag` (`tid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
